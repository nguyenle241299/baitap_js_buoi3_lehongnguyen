function tinhLuong() {
    var luongmotNgayValue = document.getElementById("luong-mot-ngay").value;

    var soNgayValue = document.getElementById("so-ngay").value;

    var result = soNgayValue * luongmotNgayValue;

    document.getElementById("result").innerHTML = ` Kết quả là: ${result}`;
}

// Bai 2

function tinhTong() {
    var realNumber1Value = parseFloat(document.getElementById("number1").value);
    var realNumber2Value = parseFloat(document.getElementById("number2").value);
    var realNumber3Value = parseFloat(document.getElementById("number3").value);
    var realNumber4Value = parseFloat(document.getElementById("number4").value);
    var realNumber5Value = parseFloat(document.getElementById("number5").value);
    
    var result2 = (realNumber1Value + realNumber2Value + realNumber3Value + realNumber4Value + realNumber5Value) / 5;
     
    document.getElementById("result-2").innerHTML = `Kết quả là: ${result2}`;

}

// Bai 3

function quyDoi() {
    var usdElement = 23500;
    var usdElementValue = document.getElementById("amount").value;

    var result3 = usdElement * usdElementValue;

    document.getElementById("result3").innerHTML = `Kết quả là: ${result3} VND`;
}

// Bai 4

function ketQua() {
    var longValue = parseFloat(document.getElementById("longs").value);
    var widthValue = parseFloat(document.getElementById("width").value);

    var areaElementValue = longValue * widthValue;
    var perimeterElementValue = (longValue + widthValue) / 2;

    document.getElementById("result-4").innerHTML = `Chu vi: ${areaElementValue}
    Diện tích: ${perimeterElementValue}`;
}

// Bai 5

function tongHaiSo() {
    var twoDegitNumber = parseFloat(document.getElementById("number").value);

    var ten = Math.floor(twoDegitNumber / 10);

    var unit = twoDegitNumber % 10;

    var result5 = ten + unit;
    console.log('result5: ', result5);

    document.getElementById("result-5").innerHTML = `Kết quả là: ${result5}`;
}

